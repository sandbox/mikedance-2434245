<?php
/**
 * @file
 * Provides administration functionality.
 */

/**
 * Provides the admin settings form.
 */
function currency_bitpay_admin_form() {
  $form = array();

  $args = array();

  $args['!link'] = l(t('click here'), 'https://bitpay.com/bitcoin-exchange-rates');
  $args['@default'] = CURRENCY_BITPAY_LINK_RATES;

  $description = t('Please specify the Bitpay exchange rates json feed, for more information please !link, defaults to @default', $args);

  $default_value = currency_bitpay_link_rates();

  $form['currency_bitpay_link_rates'] = array(
    '#type' => 'textfield',
    '#title' => t('Exchange Rates Feed'),
    '#required' => TRUE,
    '#description' => $description,
    '#default_value' => $default_value,
  );

  $intervals = array(
    0,
    60,
    180,
    300,
    600,
    900,
    1800,
    2700,
    3600,
    10800,
    21600,
    32400,
    43200,
    86400,
  );

  $options = drupal_map_assoc($intervals, 'format_interval');
  $options[0] = '<' . t('none') . '>';

  $args = array();

  $args['@default'] = $options[CURRENCY_BITPAY_EXPIRATION_RATES];

  $description = t('Please specify how long before the exchange rates should be refreshed, defaults to @default', $args);

  $default_value = currency_bitpay_expiration_rates();

  $form['currency_bitpay_expiration_rates'] = array(
    '#type' => 'select',
    '#title' => t('Exchange Rates Expiration'),
    '#description' => $description,
    '#options' => $options,
    '#default_value' => $default_value,
  );

  return system_settings_form($form);
}

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
Provides currency exchange rates into Bitcoin using the Bitpay exchange rates
from https://bitpay.com/bitcoin-exchange-rates.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/currency_bitpay

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/currency_bitpay

REQUIREMENTS
------------
This module depends upon the currency module.

RECOMMENDED MODULES
-------------------
currency

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure the module at:

 http://yoursite.com/admin/config/regional/currency-exchange/bitpay

 * Exchange rates can be viewed at:

 http://yoursite.com/admin/config/regional/currency-exchange/bitpay/rates

TROUBLESHOOTING
---------------
There are no troubleshooting recommendations at this time.

FAQ
---
There are no frequently asked questions at this time.

MAINTAINERS
-----------
Current maintainers:
 * Michael Dance (mikedance) - https://www.drupal.org/u/mikedance

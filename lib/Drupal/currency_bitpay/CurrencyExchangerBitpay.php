<?php
/**
 * @file
 * Provides the CurrencyExchangerBitpay class.
 */

namespace Drupal\currency_bitpay;

/**
 * Provides the CurrencyExchangerBitpay class.
 *
 * @package Drupal\currency_bitpay
 */
class CurrencyExchangerBitpay implements \CurrencyExchangerInterface {
  /**
   * Implements CurrencyExchangerInterface::load().
   */
  public static function load($currency_code_from, $currency_code_to) {
    $output = FALSE;

    if ($currency_code_from == 'BTC' || $currency_code_to == 'BTC') {
      if ($currency_code_from != 'BTC') {
        $inverse = TRUE;

        $code = $currency_code_from;
      }
      else {
        $inverse = FALSE;

        $code = $currency_code_to;
      }

      $result = self::loadAll();

      if (is_array($result)) {
        foreach ($result as $value) {
          if ($value->code == $code) {
            $output = $value->rate;

            if ($inverse) {
              $output = 1 / $output;
            }

            break;
          }
        }
      }
    }

    return $output;
  }

  /**
   * Implements CurrencyExchangerInterface::loadMultiple().
   */
  public static function loadMultiple(array $currency_codes) {
    $output = array();

    foreach ($currency_codes as $currency_code_from => $currency_codes_to) {
      foreach ($currency_codes_to as $currency_code_to) {
        $output[$currency_code_from][$currency_code_to] = self::load($currency_code_from, $currency_code_to);
      }
    }

    return $output;
  }

  /**
   * Retrieves the exchange rates from cache.
   */
  public static function cache() {
    $cid = 'currency_bitpay_rates';
    $bin = 'cache';

    $output = cache_get($cid, $bin);

    return $output;
  }

  /**
   * Loads all available exchange rates.
   *
   * @return array
   *   The array structure is identical to the return value of
   *   self::loadMultiple().
   */
  public static function loadAll($reset = FALSE) {
    $output = &drupal_static(__CLASS__);

    if (is_null($output) || $reset) {
      $data = NULL;

      $cache = self::cache();

      if (!$cache || $reset) {
        $url = currency_bitpay_link_rates();

        $data = file_get_contents($url);

        if ($data) {
          $cid = 'currency_bitpay_rates';
          $bin = 'cache';

          $now = time();
          $interval = currency_bitpay_expiration_rates();

          $expire = $now + $interval;

          cache_set($cid, $data, $bin, $expire);
        }
      }
      else {
        $data = $cache->data;
      }

      $output = json_decode($data);
    }

    return $output;
  }

  /**
   * Implements CurrencyExchangerInterface::operationsLinks().
   */
  public static function operationsLinks() {
    $output = array();

    $output[] = array(
      'title' => t('settings'),
      'href' => 'admin/config/regional/currency-exchange/bitpay/settings',
    );

    $output[] = array(
      'title' => t('exchange rates'),
      'href' => 'admin/config/regional/currency-exchange/bitpay/rates',
    );

    return $output;
  }

}

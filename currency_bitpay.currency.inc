<?php
/**
 * @file
 * Provides currency hooks.
 */

/**
 * Implements hook_currency_exchanger_info().
 */
function currency_bitpay_currency_exchanger_info() {
  $output = array();

  $output['CurrencyExchangerBitpay'] = array(
    'exchanger' => array(
      'class' => '\Drupal\currency_bitpay\CurrencyExchangerBitpay',
    ),
    'title' => t('Bitpay Exchange Rates'),
  );

  return $output;
}

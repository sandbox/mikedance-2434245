<?php
/**
 * @file
 * Provides helper functions.
 */

/**
 * Provides a string containing the exchange rates feed.
 *
 * The currency_bitpay_link_rates() helper function should be used to handle
 * customisations.
 */
define('CURRENCY_BITPAY_LINK_RATES', 'https://bitpay.com/api/rates');

/**
 * Provides the expiration interval for the exchange rates feed.
 *
 * Defaults to 1 minute.
 *
 * The currency_bitpay_expiration_rates() helper function should be used to
 * handle customisations.
 */
define('CURRENCY_BITPAY_EXPIRATION_RATES', 60);

/**
 * Returns a string containing the default exchange rates feed.
 */
function currency_bitpay_link_rates() {
  $output = variable_get('currency_bitpay_link_rates', CURRENCY_BITPAY_LINK_RATES);

  return $output;
}

/**
 * Returns an integer containing the default exchange rates expiration interval.
 */
function currency_bitpay_expiration_rates() {
  $output = variable_get('currency_bitpay_expiration_rates', CURRENCY_BITPAY_EXPIRATION_RATES);

  return $output;
}

<?php
/**
 * @file
 * Provides admin functionality.
 */

use \Drupal\currency_bitpay\CurrencyExchangerBitpay;

/**
 * Provides the exchange rates form.
 */
function currency_bitpay_exchange_rates_form() {
  $output = array();

  $cache = CurrencyExchangerBitpay::cache();

  if ($cache) {
    $args = array();

    $time = $cache->created;
    $time = date('Y-m-d h:i:s', $time);

    $args['@time'] = $time;

    $markup = t('The exchange rates were last retrieved at: @time', $args);

    $output['header'] = array(
      '#prefix' => '<div>',
      '#markup' => $markup,
      '#suffix' => '</div>',
    );
  }

  $output['refresh'] = array(
    '#type' => 'submit',
    '#value' => t('Refresh Exchange Rates'),
  );

  $header = array(
    t('Currency'),
    t('Code'),
    t('Conversion rate'),
  );

  $rows = array();

  $result = CurrencyExchangerBitpay::loadAll();

  foreach ($result as $value) {
    $value = (array) $value;

    $row = array();

    $row[] = check_plain($value['code']);
    $row[] = check_plain($value['name']);
    $row[] = check_plain($value['rate']);

    $rows[] = $row;
  }

  if (!$rows) {
    $data = t('There are no exchange rates available at this time.');

    $rows[] = array(
      array(
        'data' => $data,
        'colspan' => 4,
      ),
    );
  }

  $variables = array(
    'header' => $header,
    'rows' => $rows,
  );

  $markup = theme('table', $variables);

  $output['table'] = array(
    '#markup' => $markup,
  );

  return $output;
}

/**
 * Provides the bitcoin exchange rates submit handler.
 */
function currency_bitpay_exchange_rates_form_submit() {
  CurrencyExchangerBitpay::loadAll(TRUE);
}
